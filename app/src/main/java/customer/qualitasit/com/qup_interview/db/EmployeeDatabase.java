package customer.qualitasit.com.qup_interview.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import customer.qualitasit.com.qup_interview.db.module.Employee;
/**
 * Created by akash on 28-Jan-19.
 */

@Database(entities = Employee.class,version = 1)
public abstract class EmployeeDatabase extends RoomDatabase {

     private static EmployeeDatabase instance;
     public abstract EmployeeDeo employeeDeo();

     public static synchronized EmployeeDatabase getInstance(Context context)
     {
         if(instance==null)
         {
                instance= Room.databaseBuilder(context.getApplicationContext(),
                        EmployeeDatabase.class,"employee_database")
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallBack)
                        .build();
         }
        return instance;
     }
     private static RoomDatabase.Callback roomCallBack=new RoomDatabase.Callback()
     {
         @Override
         public void onCreate(@NonNull SupportSQLiteDatabase db) {
             super.onCreate(db);
             new PopulateDbAsyncTask(instance).execute();
         }
     };

     private static class PopulateDbAsyncTask extends AsyncTask<Void,Void,Void>
     {
         private EmployeeDeo employeeDeo;

         public PopulateDbAsyncTask(EmployeeDatabase database) {
             this.employeeDeo = database.employeeDeo();
         }

         @Override
         protected Void doInBackground(Void... voids) {
             employeeDeo.insert(new Employee("Akash Jagtap","Hadapasar,Pune",
                     "9552103636","akash68jagtap@gmail.com","HRMS"));
             employeeDeo.insert(new Employee("Vikas Jagtap","Swarget,Pune",
                     "9552106363","vikas68jagtap@gmail.com","YOUMI"));

             employeeDeo.insert(new Employee("Nitin Jagtap","Vagholi,Pune",
                     "9552104545","nitin68jagtap@gmail.com","Night Table"));

             return null;
         }
     }


}
