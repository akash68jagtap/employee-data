package customer.qualitasit.com.qup_interview.db;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import customer.qualitasit.com.qup_interview.db.module.Employee;

/**
 * Created by akash on 28-Jan-19.
 */
@Dao
public interface  EmployeeDeo {

    @Insert
    void insert(Employee employee);

    @Query("SELECT * FROM EMPLOYEE_TABLE ")
    LiveData<List<Employee>> getAllEmployee();
}
