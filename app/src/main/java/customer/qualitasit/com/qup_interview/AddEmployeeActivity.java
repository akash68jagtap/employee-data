package customer.qualitasit.com.qup_interview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import customer.qualitasit.com.qup_interview.db.module.Employee;
import customer.qualitasit.com.qup_interview.view_model.EmployeeViewModel;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by akash on 29-Jan-19.
 */

public class AddEmployeeActivity extends AppCompatActivity {

    @BindView(R.id.project_input_layout)
    TextInputLayout projectnameInputlayout;

    /* @BindView(R.id.address_input_layout)
     TextInputLayout addressInputlayout;


     @BindView(R.id.name_text)
     TextInputEditText nameEditText;

     @BindView(R.id.address_text)
     TextInputEditText addressEditText;*/
   private EmployeeViewModel employeeViewModel;
    @BindViews({ R.id.name_input_layout, R.id.address_input_layout, R.id.phone_input_layout, R.id.email_input_layout,
            })
    List<TextInputLayout> inputLayouts;

    boolean submitPressed=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("log0","******");
        super.onCreate(savedInstanceState);

        Log.e("log1","******");
        setContentView(R.layout.activity_add_employee);
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Log.e("log2","******");
        ButterKnife.bind(this);
        Log.e("log3","******");
        employeeViewModel= ViewModelProviders.of(this).get(EmployeeViewModel.class);
         for(TextInputLayout inputLayout:inputLayouts)
         {
             inputLayout.getEditText().addTextChangedListener(editTextWatcher);
         }
        Log.e("log4","******");
        ((MaterialButton)findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitPressed=true;
                AddEmployee();
            }
        });
    }

    TextWatcher editTextWatcher=new TextWatcher() {


        public void onTextChanged(CharSequence c, int start, int before, int count) {
            if(submitPressed)
            onTextChangeCheck();
        }

        public void beforeTextChanged(CharSequence c, int start, int count, int after) {
            // this space intentionally left blank
        }

        public void afterTextChanged(Editable c) {
            // this one too
        }
    };
    private void AddEmployee() {
        Employee employee=new Employee("","","","","");
        if(isValidForm())
        {
            for(TextInputLayout inputLayout:inputLayouts)
        {
            switch (inputLayout.getId()) {
                case R.id.name_input_layout:employee.setName(inputLayout.getEditText().getText().toString());break;
                case R.id.address_input_layout:employee.setAddress(inputLayout.getEditText().getText().toString());break;
                case R.id.phone_input_layout:employee.setPhoneNumber(inputLayout.getEditText().getText().toString());break;
                case R.id.email_input_layout:employee.setEmailId(inputLayout.getEditText().getText().toString());break;

            }
        }
        employee.setProjectName(projectnameInputlayout.getEditText().getText().toString());
        employeeViewModel.insert(employee);
            finish();

        }
    }


    private boolean isValidForm()
    {
         boolean validForm=true;


        Validation validation=new Validation();

        for(TextInputLayout textInputLayout:inputLayouts)
        {
            if(!validation.isEmpty(textInputLayout))
            {
                validForm=false;
                break;
            };
            if(textInputLayout.getId()==R.id.email_input_layout)
            {
                if(!validation.isValidEmail(textInputLayout))
                {
                    validForm=false;
                    break;
                };

            }

        }

       /* if(validation.isEmpty(nameInputlayout))
           if(validation.isEmpty(addressInputlayout))
              validForm=true;*/

           return validForm;
    }

    private void onTextChangeCheck()
    {
        Validation validation=new Validation();
        for(TextInputLayout textInputLayout:inputLayouts)
        {
            validation.isEmpty(textInputLayout);
            if(textInputLayout.getId()==R.id.email_input_layout)
            {
                validation.isValidEmail(textInputLayout);
            }
        }

    }
    class Validation
    {
        public  boolean isEmpty(TextInputLayout textInputLayout)
        {
            if(textInputLayout.getEditText().getText().toString().equals(""))
            {
                textInputLayout.setError("Cannot be empty");
                textInputLayout.setFocusable(true);
                return  false;
            }
            else
            {
                textInputLayout.setErrorEnabled(false);
                return  true;
            }
        }
        private boolean isValidEmail(TextInputLayout textInputLayout){
            boolean isValidEmail = false;

            String regExpn = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            CharSequence inputStr = textInputLayout.getEditText().getText();

            Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (!textInputLayout.getEditText().getText().toString().matches(regExpn))
            {
                textInputLayout.setError("Enter Valid Email");
                textInputLayout.setFocusable(true);
                return  false;
            }
            else
            {
                textInputLayout.setErrorEnabled(false);
                return  true;
            }

        }
    }
}
