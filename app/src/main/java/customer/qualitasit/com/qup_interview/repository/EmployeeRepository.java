package customer.qualitasit.com.qup_interview.repository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import customer.qualitasit.com.qup_interview.db.EmployeeDatabase;
import customer.qualitasit.com.qup_interview.db.EmployeeDeo;
import customer.qualitasit.com.qup_interview.db.module.Employee;

/**
 * Created by akash on 28-Jan-19.
 */
public class EmployeeRepository {

    private EmployeeDeo employeeDeo;
    private LiveData<List<Employee>> employeeList;

    public  EmployeeRepository(Application application)
    {
        EmployeeDatabase database=EmployeeDatabase.getInstance(application);
        employeeDeo=database.employeeDeo();
        employeeList=employeeDeo.getAllEmployee();
    }
    public void insert(Employee employee)
    {
        (new InsertEmployeesAsyncTast(employeeDeo)).execute(employee);
    }
    public LiveData<List<Employee>> getEmployeeList()
    {
        return employeeList;
    }
    public static class InsertEmployeesAsyncTast extends AsyncTask<Employee,Void,Void>
    {
          private EmployeeDeo employeeDeo;

        public InsertEmployeesAsyncTast(EmployeeDeo employeeDeo) {
            this.employeeDeo = employeeDeo;
        }

        @Override
        protected Void doInBackground(Employee... employees) {
            employeeDeo.insert(employees[0]);
            return null;
        }
    }

}
