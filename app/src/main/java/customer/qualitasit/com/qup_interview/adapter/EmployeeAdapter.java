package customer.qualitasit.com.qup_interview.adapter;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import customer.qualitasit.com.qup_interview.R;
import customer.qualitasit.com.qup_interview.db.module.Employee;
/**
 * Created by akash on 29-Jan-19.
 */

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> {

    private List<Employee> employeeList=new ArrayList<>();

    class EmployeeViewHolder extends RecyclerView.ViewHolder
    {

        TextView textName,textAddr,textPhone,textEmail,textProject;
        public EmployeeViewHolder(@NonNull View itemView) {
            super(itemView);
            textName=itemView.findViewById(R.id.textName);
            textAddr=itemView.findViewById(R.id.textAddr);
            textPhone=itemView.findViewById(R.id.textPhone);
            textEmail=itemView.findViewById(R.id.textEmail);
            textProject=itemView.findViewById(R.id.textProject);

        }
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employee_item,parent,false);
        return  new EmployeeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, int position) {
        Employee employee=employeeList.get(position);
        holder.textName.setText(employee.getName());
        holder.textAddr.setText(employee.getAddress());


        holder.textPhone.setText(getSpannableString("Phone Number:",employee.getPhoneNumber()));
        holder.textEmail.setText(getSpannableString("Email:",employee.getEmailId()));
        if(employee.getProjectName()!=null && !employee.getProjectName().equals(""))
        holder.textProject.setText(getSpannableString("Project Name:",employee.getProjectName()));
        else
            holder.textProject.setVisibility(View.GONE);
    }

    private SpannableStringBuilder getSpannableString(String title,String value)
    {
        SpannableStringBuilder builder= new SpannableStringBuilder();
        StyleSpan boldSpan = new StyleSpan(android.graphics.Typeface.BOLD);
        builder.append(title, boldSpan, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                .append(value);
        return builder;
    }

    public void setEmployeeList(List<Employee> employeeList) {

        try {
            this.employeeList=employeeList;
            this.notifyDataSetChanged();
        }
        catch (Exception ex)
        {
            String str="";
        }

    }


    @Override
    public int getItemCount() {
        return employeeList.size();
    }
}
