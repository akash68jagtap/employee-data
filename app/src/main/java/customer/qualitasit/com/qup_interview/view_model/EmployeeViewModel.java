package customer.qualitasit.com.qup_interview.view_model;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import customer.qualitasit.com.qup_interview.db.module.Employee;
import customer.qualitasit.com.qup_interview.repository.EmployeeRepository;

/**
 * Created by akash on 28-Jan-19.
 */

public class EmployeeViewModel extends AndroidViewModel {

    private EmployeeRepository repository;
    private LiveData<List<Employee>>  employeeList;
    public EmployeeViewModel(@NonNull Application application) {
        super(application);
        repository=new EmployeeRepository(application);
        employeeList=repository.getEmployeeList();
    }
    public void insert(Employee employee)
    {
        repository.insert(employee);

    }
    public  LiveData<List<Employee>> getEmployeeList()
    {
        return employeeList;
    }
}
