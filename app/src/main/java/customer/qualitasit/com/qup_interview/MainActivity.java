package customer.qualitasit.com.qup_interview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import customer.qualitasit.com.qup_interview.adapter.EmployeeAdapter;
import customer.qualitasit.com.qup_interview.db.module.Employee;
import customer.qualitasit.com.qup_interview.view_model.EmployeeViewModel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

/**
 * Created by akash on 28-Jan-19.
 */

public class MainActivity extends AppCompatActivity {

    private EmployeeViewModel employeeViewModel;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView=findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setSupportActionBar(findViewById(R.id.toolbar));
        mContext=this;
      //  recyclerView.setHasFixedSize(true);

        final EmployeeAdapter adapter=new EmployeeAdapter();
        recyclerView.setAdapter(adapter);
        employeeViewModel= ViewModelProviders.of(this).get(EmployeeViewModel.class);
        employeeViewModel.getEmployeeList().observe(this, new Observer<List<Employee>>() {
            @Override
            public void onChanged(List<Employee> employees) {
                adapter.setEmployeeList(employees);

            }
        });
        ((FloatingActionButton)findViewById(R.id.floatingActionButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("log0.1","******");
                startActivity(new Intent(mContext,AddEmployeeActivity.class));
                Log.e("log0.2","******");
            }
        });
    }
}
